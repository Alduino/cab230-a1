# CAB230: Happiness API Client - Hapico Foundation

This website was created for Assignment 1 of CAB230 at QUT. This README is a shortened version of the original report, only including the user guide.

If you are doing CAB230 at QUT, obviously don't go looking through this code. That is cheating and you will be caught.

## User guide

### Build & Run

Note: Please make sure the `.env.local` file exists before building. The map will not work without this, as it sets the access token that is used.

```shell
# Built with [pnpm](https://pnpm.io), use that instead if you have it installed
npm i
npm run build
npx serve -s build # -s flag sets SPA mode - all requests that don't resolve to another file resolve to index.html
```

See `serve` output for URL

### Usage

Note that the application caches all requests (except for login and registration requests), so it can be used offline for data you have already seen before.

#### Search

To search for a country, click on the `Find a country` input in the navigation bar. Start typing your query in the textbox that appears, and click on the option you want in the results.

The search box also supports a few keyboard shortcuts:

- <kbd>Tab</kbd> fills in the autocompleted content
- <kbd>↑</kbd> and <kbd>↓</kbd> move the selected result
- <kbd>Enter</kbd> acts the same as clicking on the selected result

Press <kbd>Esc</kbd> or click outside of the popup to close the search box.

#### Global ranking

Navigate to the global rankings page by clicking on 'Rankings' in the navigation bar.

There are three main components here: the year selector, the map, and the ranking list.

##### Year selector

Selects the year that's data is shown.

Use arrow keys to navigate through these with your keyboard. Note that you should use your mouse to initially focus on the selector - see difficulties section above.

##### Map

This map shows the scores of each country, where green is a high score and red is a low score. You can move around on this map using the standard controls (click and drag, arrow keys when focused), and you can scroll to zoom, as well as right click and drag to rotate. To get back to vertical, rotate so the map is close to vertical, and it will ease back to the exact rotation.

##### Ranking list

This list shows each country in order of their rank for the selected year's rankings.

Clicking on an item in this list will navigate to its country ranking page.

#### Country ranking

Navigate to this page through the search or the global rankings page's sidebar.

This page shows a graph of rank and score vs year and, if logged in, the factors that went into the score, for each year.

##### Graph

In the light theme, the blue line is the happiness score (maps to the values on the left), and the red line is the rank (maps to the values on the right).

In the dark theme, the green line is the rank.

The blue vertical line shows the year currently selected in the factors display.

##### Factors display

If you are not logged in, the text "Log in to view more detail" will show here. Click the text "Log in" to navigate to the login page. Logging in here will redirect you back to this country's page.

If you are logged in, the factors display will be shown below the graph. This display shows how much each happiness factor contributed to the final score. You can select any year to view the score for using the year selector (which functions the same as in the global ranking page).

#### Top factors

Navigate to the top factors page with the 'Factors' button in the navigation bar.

Accessing this page while logged out will redirect to the login page, which will redirect back after logging in successfully.

This page shows a factors view similar to the individual country ranking. It uses data from the top 10 countries to reduce the amount of network load.

You can select a different year to view the data from that year.

#### Accounts

Note that, if your access token expires, you will be automatically logged out of your account the next time you try to access an authenticated page.

##### Logging in

Log in by clicking on the "Log in" button in the navigation bar.

Enter your email and password, then press <kbd>Enter</kbd> or click the submit button.

If your details are correct, the button will show "Logged in", and will redirect you after two seconds.

If the details are not correct, the button will show "Incorrect credentials" for two seconds.

If some other error occurred, the button will say so, and you will have to refresh the page to try again.

##### Registering

This is the same flow as logging in, accessible through the green "Register" button in the navigation bar.

If someone is already using the specified email, you will get a message saying "Account already exists".

Otherwise, and if nothing else goes wrong, the account will be created, and you will be redirected to the login page after two seconds.

##### Logging out

If you are logged in, your email will show in the navigation bar, with a "Log out" button beside it. Click this button to get to the log out confirmation page.

If you click 'No', you will not be logged out and you will be redirected back to the previous page.

If you click 'Yes', you will be logged out, and redirected to the home page.
