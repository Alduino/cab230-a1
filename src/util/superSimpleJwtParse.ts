import b64 from "base64url";

interface JWT {
    exp: number;
    iat: number;
    email: string;
}

export default function superSimpleJwtParse(jwt: string): JWT {
    const parts = jwt.split(".");
    if (parts.length !== 3) throw new Error("Invalid JWT");
    const [, claims] = parts;
    return JSON.parse(b64.decode(claims));
}
