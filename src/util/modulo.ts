/**
 * Modulo operator with correct handling of negative values
 * @see https://stackoverflow.com/a/1082938
 */
export default function modulo(x: number, m: number) {
    return ((x % m) + m) % m;
}
