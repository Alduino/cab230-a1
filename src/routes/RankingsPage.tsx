import {
    Box,
    Center,
    Heading,
    Spinner,
    Stack,
    Text,
    useColorModeValue,
    useToken,
    VStack
} from "@chakra-ui/react";
import interpolate from "color-interpolate";
import countries from "i18n-iso-countries";
import enLocale from "i18n-iso-countries/langs/en.json";
import mapboxgl, {
    Expression,
    Map as Mapbox,
    MapboxGeoJSONFeature
} from "mapbox-gl";
import React, {
    memo,
    ReactElement,
    useCallback,
    useEffect,
    useMemo,
    useState
} from "react";
import {ButtonSlider} from "../components/ButtonSlider";
import {ErrorText} from "../components/ErrorText";
import {Link} from "../components/Link";
import {useRankings} from "../hooks/api";
import {useRisingCutValue} from "../hooks/cutValue";
import "mapbox-gl/dist/mapbox-gl.css";
import usePrevious from "../hooks/previous";

// Fixes build issue with Mapbox v2
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
// eslint-disable-next-line @typescript-eslint/no-var-requires,import/no-webpack-loader-syntax, import/no-unresolved
mapboxgl.workerClass = require("worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker").default;

countries.registerLocale(enLocale);

interface RankingsDisplayProps {
    year: number;
    highlightedCountry: string | undefined;
}

/**
 * Maps data name to lib name
 */
function getFullName(country: string) {
    const mapping: Record<string, string> = {
        "United States": "United States of America",
        Moldova: "Moldova, Republic of",
        "North Cyprus": "Cyprus", // not supported
        "Northern Cyprus": "Cyprus", // not supported
        "Ivory Coast": "Cote D'Ivoire",
        "Congo (Brazzaville)": "Congo, the Democratic Republic of the",
        Macedonia: "North Macedonia, Republic of",
        "North Macedonia": "North Macedonia, Republic of",
        Laos: "Lao People's Democratic Republic",
        Iran: "Iran, Islamic Republic of",
        "Palestinian Territories": "Palestine",
        "Congo (Kinshasa)": "Congo",
        Swaziland: "Eswatini",
        Tanzania: "Tanzania, United Republic of",
        Syria: "Syrian Arab Republic",
        "Somaliland region": "Somalia", // not supported
        "Somaliland Region": "Somalia" // not supported
    };

    return mapping[country] || country;
}

const RankingsDisplay = ({year, highlightedCountry}: RankingsDisplayProps) => {
    const [rankings, rankingsError] = useRankings({year});
    const cutRankings = useRisingCutValue(rankings, !!rankings);
    const previousHighlightedCountry = usePrevious(highlightedCountry);
    const [sourceFeatures, setSourceFeatures] = useState<
        MapboxGeoJSONFeature[]
    >([]);

    const mapColour = interpolate([
        useToken("colors", "red.600"),
        useToken("colors", "yellow.400"),
        useToken("colors", "green.300")
    ]);

    const backgroundColour = useColorModeValue(
        "blackAlpha.200",
        "whiteAlpha.200"
    );

    const rankingsWithA3 = useMemo(
        () =>
            // convert to Map keyed by a3 and back to array to make key distinct
            Array.from(
                new Map(
                    cutRankings?.map(data => {
                        const a3 = countries.getAlpha3Code(
                            getFullName(data.country),
                            "en"
                        );

                        return [a3, {...data, a3}];
                    })
                ).values()
            ),
        [cutRankings]
    );

    const [mapContainer, setMapContainer] = useState<HTMLElement | null>(null);
    const [map, setMap] = useState<Mapbox>();

    const {name2Id} = useMemo(() => {
        const name2Id = new Map<string, string | number>();
        const id2Name = new Map<string | number, string>();

        for (const feature of sourceFeatures) {
            if (!feature.properties) continue;
            if (typeof feature.id === "undefined") continue;

            const alpha3: string = feature.properties.iso_3166_1_alpha_3;
            name2Id.set(alpha3, feature.id);
            id2Name.set(feature.id, alpha3);
        }

        return {name2Id, id2Name};
    }, [sourceFeatures]);

    useEffect(() => {
        if (!map) return;
        if (!rankingsWithA3) return;

        const minScore = Math.min(...rankingsWithA3.map(el => el.score));
        const maxScore = Math.max(...rankingsWithA3.map(el => el.score));

        const matchExpr = [
            "match",
            ["get", "iso_3166_1_alpha_3"],
            ...rankingsWithA3.flatMap(rank => {
                if (!rank.a3)
                    throw new Error(
                        `${rank.country} (${getFullName(
                            rank.country
                        )}) has no a3!`
                    );
                // put within a 0-1 range
                const idx = (rank.score - minScore) / (maxScore - minScore);
                const colour = mapColour(idx);
                return [rank.a3, colour];
            }),
            `rgba(0, 0, 0, 0)`
        ] as Expression;

        try {
            map.setPaintProperty("colour-layer", "fill-color", matchExpr);
        } catch {
            // don't worry about errors
        }
    }, [map, rankingsWithA3]);

    useEffect(() => {
        if (!map) return;

        const matchExpr = [
            "case",
            ["boolean", ["feature-state", "hover"], false],
            "rgba(0,0,0,0.3)",
            "rgba(0,0,0,0)"
        ];

        try {
            map.setPaintProperty("highlight-layer", "fill-color", matchExpr);
        } catch {
            // don't worry about errors
        }
    }, [map]);

    useEffect(() => {
        if (!map) return;

        if (typeof previousHighlightedCountry !== "undefined") {
            const previousId = name2Id.get(previousHighlightedCountry);

            if (typeof previousId !== "undefined") {
                map.setFeatureState(
                    {
                        source: "country-boundaries",
                        sourceLayer: "country_boundaries",
                        id: previousId
                    },
                    {hover: false}
                );
            }
        }

        if (typeof highlightedCountry !== "undefined") {
            const id = name2Id.get(highlightedCountry);

            if (typeof id !== "undefined") {
                map.setFeatureState(
                    {
                        source: "country-boundaries",
                        sourceLayer: "country_boundaries",
                        id
                    },
                    {hover: true}
                );
            }
        }
    }, [map, previousHighlightedCountry, highlightedCountry]);

    useEffect(() => {
        if (!mapContainer) return;

        const map = new Mapbox({
            accessToken: process.env.REACT_APP_MAPBOX_ACCESS_TOKEN as string,
            style: "mapbox://styles/mapbox/light-v10",
            container: mapContainer,
            zoom: 1.6
        });

        map.on("load", () => {
            map.addSource("country-boundaries", {
                type: "vector",
                url: "mapbox://mapbox.country-boundaries-v1"
            });

            map.addLayer({
                id: "sky",
                type: "sky"
            });

            map.addLayer(
                {
                    id: "colour-layer",
                    source: "country-boundaries",
                    "source-layer": "country_boundaries",
                    type: "fill",
                    paint: {
                        "fill-color": "rgba(0,0,0,0)"
                    }
                },
                "country-label"
            );

            map.addLayer(
                {
                    id: "highlight-layer",
                    source: "country-boundaries",
                    "source-layer": "country_boundaries",
                    type: "fill",
                    paint: {
                        "fill-color": "rgba(0,0,0,0)"
                    }
                },
                "country-label"
            );

            map.on("sourcedata", ev => {
                if (ev.sourceId !== "country-boundaries") return;
                if (!ev.isSourceLoaded) return;
                setSourceFeatures(
                    map.querySourceFeatures("country-boundaries", {
                        sourceLayer: "country_boundaries"
                    })
                );
            });

            setMap(map);
        });

        return () => {
            setMap(undefined);
            map.remove();
        };
    }, [mapContainer]);

    if (rankingsError) {
        return <ErrorText error={rankingsError} />;
    } else if (cutRankings) {
        return (
            <Box position="relative" width="full">
                <Center
                    width="full"
                    height="full"
                    position="absolute"
                    bg={backgroundColour}
                >
                    <Spinner />
                </Center>
                <Box
                    borderRadius="md"
                    width="full"
                    height="full"
                    position="absolute"
                    ref={setMapContainer}
                />
            </Box>
        );
    } else {
        return (
            <Center width="full" bg={backgroundColour}>
                <Spinner />
            </Center>
        );
    }
};

interface RankedCountryListProps {
    year: number;
    setHighlightedCountry(country: string | undefined): void;
}

const RankedCountryList = memo(
    ({year, setHighlightedCountry}: RankedCountryListProps) => {
        const [ranks, ranksError] = useRankings({year});

        const orderedRanks = useMemo(
            () => ranks?.sort((a, b) => a.rank - b.rank),
            [ranks]
        );

        const backgroundColour = useColorModeValue(
            "blackAlpha.200",
            "whiteAlpha.200"
        );

        const linkHoverColour = useColorModeValue(
            "blackAlpha.200",
            "whiteAlpha.200"
        );

        const handleMouseEnter = useCallback(
            (country: string) => {
                const alpha3 = countries.getAlpha3Code(
                    getFullName(country),
                    "en"
                );
                setHighlightedCountry(alpha3);
            },
            [setHighlightedCountry]
        );

        if (ranksError) {
            return <ErrorText error={ranksError} />;
        } else if (!orderedRanks) {
            return (
                <Center w={96} bg={backgroundColour} borderRadius="md">
                    <Spinner />
                </Center>
            );
        }

        return (
            <Stack
                w={96}
                overflowY="auto"
                spacing={0}
                bg={backgroundColour}
                borderRadius="md"
                onMouseOut={() => setHighlightedCountry(undefined)}
            >
                {orderedRanks.map((rank, i) => (
                    <Link
                        _hover={{
                            textDecoration: "none",
                            background: linkHoverColour
                        }}
                        href={`/rankings/${rank.country}`}
                        useDefaultStyle={false}
                        key={i}
                    >
                        <Stack
                            direction="row"
                            alignItems="top"
                            px={4}
                            py={2}
                            onMouseOver={() => handleMouseEnter(rank.country)}
                        >
                            <Text fontWeight="bold">{rank.rank}.</Text>
                            <Text>{rank.country}</Text>
                        </Stack>
                    </Link>
                ))}
            </Stack>
        );
    }
);

const RankingsPage = (): ReactElement => {
    const [targetYear, setTargetYear] = useState(2020);
    const [highlightedCountry, setHighlightedCountry] = useState<
        string | undefined
    >();

    return (
        <Stack spacing={8}>
            <VStack spacing={2}>
                <Heading>Happiness Rankings</Heading>
                <Text>Green countries have higher rankings</Text>
            </VStack>

            <Stack direction="row" spacing={8} height="32rem">
                <ButtonSlider
                    label="Display year"
                    min={2015}
                    max={2020}
                    orientation="vertical"
                    value={targetYear}
                    onChange={setTargetYear}
                />
                <RankingsDisplay
                    year={targetYear}
                    highlightedCountry={highlightedCountry}
                />
                <RankedCountryList
                    year={targetYear}
                    setHighlightedCountry={setHighlightedCountry}
                />
            </Stack>
        </Stack>
    );
};

export default RankingsPage;
