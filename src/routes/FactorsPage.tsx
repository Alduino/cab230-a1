import {
    Box,
    Center,
    chakra,
    Heading,
    HStack,
    Spinner,
    Stack,
    Text,
    VStack
} from "@chakra-ui/react";
import React, {ReactElement, useMemo, useState} from "react";
import {ButtonSlider} from "../components/ButtonSlider";
import {ErrorText} from "../components/ErrorText";
import {FactorDisplay} from "../components/FactorDisplay";
import {RankingWithFactors, useFactors} from "../hooks/api";
import {useRisingCutValue} from "../hooks/cutValue";
import useRequiredAuthentication from "../hooks/requiredAuthentication";
import ClassNameProps from "../util/ClassNameProps";

interface TopFactorsDisplay extends ClassNameProps {
    year: number;
}

function avg<T>(arr: T[], map: (el: T) => number) {
    if (arr.length === 0) return 0;
    const total = arr.map(map).reduce((a, b) => a + b);
    return total / arr.length;
}

function useAverageFactors(factors?: RankingWithFactors[]) {
    const economy = avg(factors ?? [], el => el.economy);
    const family = avg(factors ?? [], el => el.family);
    const health = avg(factors ?? [], el => el.health);
    const freedom = avg(factors ?? [], el => el.freedom);
    const generosity = avg(factors ?? [], el => el.generosity);
    const trust = avg(factors ?? [], el => el.trust);

    return {economy, family, health, freedom, generosity, trust};
}

const TopFactorsDisplay = chakra(
    ({year, className}: TopFactorsDisplay): ReactElement => {
        const [factors, factorsError] = useFactors({year, limit: 10});
        const averages = useAverageFactors(factors);
        const hasFactors = useRisingCutValue(!!factors, !!factors);
        const averagesEdge = useRisingCutValue(averages, !!factors);

        if (factorsError) {
            return <ErrorText error={factorsError} />;
        } else if (!hasFactors) {
            return (
                <Center>
                    <Spinner />
                </Center>
            );
        } else {
            return <FactorDisplay {...averagesEdge} className={className} />;
        }
    }
);

const FactorsPage = (): ReactElement => {
    useRequiredAuthentication();

    const [selectedYear, setSelectedYear] = useState(2020);

    return (
        <Stack spacing={8}>
            <VStack>
                <Heading>Top Factors</Heading>
                <Text>
                    The factors that citizens find the most important in the top
                    10 countries
                </Text>
            </VStack>

            <Stack spacing={0}>
                <ButtonSlider
                    label="Selected year"
                    min={2015}
                    max={2020}
                    borderBottomRadius={0}
                    borderBottomWidth="1px"
                    borderBottomColor="gray.600"
                    value={selectedYear}
                    onChange={setSelectedYear}
                />

                <TopFactorsDisplay borderTopRadius={0} year={selectedYear} />
            </Stack>
        </Stack>
    );
};

export default FactorsPage;
