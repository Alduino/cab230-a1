import {Heading, HStack, Stack, Text, VStack} from "@chakra-ui/react";
import React, {FC} from "react";
import {AiFillDatabase} from "react-icons/ai";
import {BiPlanet} from "react-icons/all";
import {SiGlassdoor} from "react-icons/si";
import {Hero} from "../components/Hero";
import {HeroItem, VerticalDivider} from "../components/HeroItem";

const LandingPage: FC = () => (
    <Stack spacing={8}>
        <VStack>
            <Heading size="md">Welcome to the Hapico Foundation</Heading>
        </VStack>

        <Hero>
            <HStack spacing={4} align="stretch">
                <HeroItem heading="High quality data" icon={<AiFillDatabase />}>
                    <Text>
                        We strive to collect only the highest quality data, with
                        help from world-wide agencies, like the United Nations.
                    </Text>
                </HeroItem>
                <VerticalDivider />
                <HeroItem heading="Transparent and open" icon={<SiGlassdoor />}>
                    <Text>
                        The Hapico Foundation is built on transparency, which is
                        why nearly all of our data is available for free,
                        without an account.
                    </Text>
                </HeroItem>
                <VerticalDivider />
                <HeroItem heading="For the planet" icon={<BiPlanet />}>
                    <Text>
                        We want everyone to be happy, and enjoy their life,
                        because then we can pursue ways to save the planet, and
                        improve our lives further.
                    </Text>
                </HeroItem>
            </HStack>
        </Hero>
    </Stack>
);

export default LandingPage;
