import {
    Box,
    Center,
    Heading,
    HStack,
    Spinner,
    Stack,
    Text,
    useColorModeValue,
    useToken,
    VStack
} from "@chakra-ui/react";
import {ChartData, Chart as ChartJS} from "chart.js";
import annotationPlugin from "chartjs-plugin-annotation";
import React, {ReactElement, useState} from "react";
import Chart from "react-chartjs-2";
import {useRouteMatch} from "react-router";
import {ButtonSlider} from "../components/ButtonSlider";
import {ErrorText} from "../components/ErrorText";
import {FactorDisplay} from "../components/FactorDisplay";
import {Link} from "../components/Link";
import {
    Ranking,
    useAuthenticationStatus,
    useFactors,
    useRankings
} from "../hooks/api";
import {useRisingCutValue} from "../hooks/cutValue";

ChartJS.register(annotationPlugin);

const RankingGraph = ({
    rankings,
    activeYear
}: {
    rankings: Ranking[];
    activeYear: number;
}) => {
    const scoreColour = useToken(
        "colors",
        useColorModeValue("blue.600", "blue.400")
    );
    const rankColour = useToken(
        "colors",
        useColorModeValue("red.600", "green.400")
    );

    const borderLineColour = useToken(
        "colors",
        useColorModeValue("blackAlpha.300", "whiteAlpha.300")
    );

    const yearLineColour = useToken(
        "colors",
        useColorModeValue("blue.300", "blue.700")
    );

    const data: ChartData = {
        labels: rankings.map(ranking => ranking.year.toString()),
        datasets: [
            {
                label: "Happiness Score",
                data: rankings.map(item => item.score),
                backgroundColor: scoreColour,
                borderColor: scoreColour,
                yAxisID: "score",
                xAxisID: "year",
                tension: 0.3
            },
            {
                label: "Rank",
                data: rankings.map(item => item.rank),
                backgroundColor: rankColour,
                borderColor: rankColour,
                yAxisID: "rank",
                xAxisID: "year",
                tension: 0.3
            }
        ]
    };

    return (
        <HStack>
            <Chart
                type="line"
                data={data}
                height={128}
                options={{
                    animation: {
                        duration: 0
                    },
                    plugins: {
                        annotation: {
                            annotations: {
                                yearLine: {
                                    type: "line",
                                    scaleID: "year",
                                    value: activeYear,
                                    endValue: activeYear,
                                    borderColor: yearLineColour,
                                    borderWidth: 8,
                                    drawTime: "beforeDatasetsDraw"
                                }
                            }
                        }
                    },
                    scales: {
                        year: {
                            type: "linear",
                            position: "bottom",
                            grid: {
                                borderColor: borderLineColour,
                                color: borderLineColour
                            },
                            title: {
                                display: true,
                                text: "Year"
                            },
                            ticks: {
                                callback(value: number) {
                                    if (value !== Math.floor(value))
                                        return null;

                                    return value.toString();
                                }
                            }
                        },
                        score: {
                            type: "linear",
                            position: "left",
                            grid: {
                                borderColor: borderLineColour,
                                color: borderLineColour
                            },
                            title: {
                                display: true,
                                text: "Score"
                            }
                        },
                        rank: {
                            type: "linear",
                            position: "right",
                            reverse: true,
                            grid: {
                                drawOnChartArea: false,
                                borderColor: borderLineColour,
                                color: borderLineColour
                            },
                            title: {
                                display: true,
                                text: "Rank"
                            },
                            ticks: {
                                callback(value: number) {
                                    if (value !== Math.floor(value))
                                        return null;

                                    const onesDigit = value % 10;
                                    const tensDigit = Math.floor(value / 10);

                                    if (tensDigit === 1) return `${value}th`;

                                    switch (onesDigit) {
                                        case 1:
                                            return `${value}st`;
                                        case 2:
                                            return `${value}nd`;
                                        case 3:
                                            return `${value}rd`;
                                        default:
                                            return `${value}th`;
                                    }
                                }
                            }
                        }
                    }
                }}
            />
        </HStack>
    );
};

const FactorsDetail = ({
    country,
    year,
    setYear
}: {
    country: string;
    year: number;
    setYear(v: number): void;
}) => {
    const [factorsList, factorsError] = useFactors({year, country});
    const factorsEdge = useRisingCutValue(factorsList?.[0], !!factorsList);

    if (factorsError) {
        return <ErrorText error={factorsError} />;
    } else if (!factorsEdge) {
        return (
            <Center>
                <Spinner />
            </Center>
        );
    }

    return (
        <Stack spacing={0}>
            <ButtonSlider
                label="Selected year"
                min={2015}
                max={2020}
                borderBottomRadius={0}
                borderBottomWidth="1px"
                borderBottomColor="gray.600"
                value={year}
                onChange={setYear}
            />

            <FactorDisplay {...factorsEdge} borderTopRadius={0} year={year} />
        </Stack>
    );
};

const CountryRankingPage = (): ReactElement => {
    const {
        params: {country}
    } = useRouteMatch<{country: string}>();

    const [activeYear, setActiveYear] = useState(2020);

    const [rankings, rankingsError] = useRankings({country});

    const loggedIn = useAuthenticationStatus();

    if (rankingsError) {
        return <ErrorText error={rankingsError} />;
    } else if (!rankings) {
        return (
            <Center>
                <Spinner />
            </Center>
        );
    } else if (rankings.length === 0) {
        return (
            <Box>
                <Heading size="md">Not found</Heading>
                <Text>
                    We don&rsquo;t seem to have {country} in our database.
                </Text>
            </Box>
        );
    }

    const rankingsOrdered = rankings.sort((a, b) => a.year - b.year);

    return (
        <Stack spacing={8}>
            <VStack>
                <Heading size="md">{country}</Heading>
            </VStack>

            <RankingGraph rankings={rankingsOrdered} activeYear={activeYear} />
            {loggedIn ? (
                <FactorsDetail
                    country={country}
                    year={activeYear}
                    setYear={setActiveYear}
                />
            ) : (
                <Text alignSelf="center">
                    <Link href={`/account/login?return=/rankings/${country}`}>
                        Log in
                    </Link>{" "}
                    to view more detail
                </Text>
            )}
        </Stack>
    );
};

export default CountryRankingPage;
