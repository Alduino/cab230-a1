import React, {ReactElement, useMemo, useState} from "react";
import {AccountBox} from "../../components/AccountBox";
import {useDeferredRegister} from "../../hooks/api";

const Register = (): ReactElement => {
    const [fields, setFields] = useState<Record<string, string>>({});

    const fieldsOrEmpty = useMemo(
        () => ({
            email: fields.email ?? "",
            password: fields.password ?? ""
        }),
        [fields.email, fields.password]
    );

    const register = useDeferredRegister(fieldsOrEmpty);

    return (
        <AccountBox
            title="Register"
            description="Registered users can access more detailed data"
            completeLabel="Account created"
            errorLabel="Account already exists"
            successRedirect="/account/login"
            deferredHandler={register}
            onFieldsChanged={setFields}
        />
    );
};

export default Register;
