import React, {ReactElement, useMemo, useState} from "react";
import {useLocation} from "react-router";
import {AccountBox} from "../../components/AccountBox";
import {useDeferredLogIn} from "../../hooks/api";

const LogIn = (): ReactElement => {
    const [fields, setFields] = useState<Record<string, string>>({});

    const fieldsOrEmpty = useMemo(
        () => ({
            email: fields.email ?? "",
            password: fields.password ?? ""
        }),
        [fields.email, fields.password]
    );

    const logIn = useDeferredLogIn(fieldsOrEmpty);

    const {search} = useLocation();
    const returnLocation = new URLSearchParams(search).get("return");

    return (
        <AccountBox
            title="Log in"
            description="Access higher detail information"
            completeLabel="Logged in"
            errorLabel="Incorrect credentials"
            successRedirect={returnLocation || "/"}
            deferredHandler={logIn}
            onFieldsChanged={setFields}
        />
    );
};

export default LogIn;
