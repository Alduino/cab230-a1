import {
    Button,
    Center,
    HStack,
    VStack,
    Text,
    useColorModeValue
} from "@chakra-ui/react";
import React, {ReactElement, useCallback} from "react";
import {useHistory} from "react-router";
import {useDeferredLogOut} from "../../hooks/api";

const Logout = (): ReactElement => {
    const {push: historyPush, goBack} = useHistory();
    const backgroundColour = useColorModeValue("gray.100", "gray.700");
    const logOut = useDeferredLogOut();

    const handleLogOut = useCallback(() => {
        logOut();
        historyPush("/");
    }, [logOut, historyPush]);

    return (
        <Center>
            <VStack
                bg={backgroundColour}
                w="26rem"
                borderRadius="md"
                px={4}
                py={8}
                spacing={4}
            >
                <Text>Are you sure you want to log out?</Text>
                <HStack>
                    <Button colorScheme="green" onClick={handleLogOut}>
                        Yes
                    </Button>
                    <Button onClick={goBack}>No</Button>
                </HStack>
            </VStack>
        </Center>
    );
};

export default Logout;
