import {chakra, Image} from "@chakra-ui/react";
import React from "react";

export interface LogoProps {
    className?: string;
}

export const Logo = chakra(({className}: LogoProps) => (
    <Image src="/logo.svg" className={className} />
));
