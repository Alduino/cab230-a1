import assert from "assert";
import {useCallback, useEffect, useMemo, useRef, useState} from "react";
import UseAsyncResult from "./UseAsyncResult";
import UseDeferredAsyncResult from "./UseDeferredAsyncResult";

type Timeout = false | number;

export interface UseDeferredAsyncOptions<IsAborting extends Timeout = false> {
    timeout?: IsAborting;
}

export function useDeferredAsync<
    Result,
    Arguments extends unknown[],
    TimeoutTime extends Timeout
>(
    fn: (
        ...args: TimeoutTime extends number
            ? [...Arguments, AbortSignal]
            : Arguments
    ) => Promise<Result>,
    deps: Arguments,
    opts: UseDeferredAsyncOptions<TimeoutTime> = {}
): UseDeferredAsyncResult<Result> {
    const [
        abortController,
        setAbortController
    ] = useState<AbortController | null>(null);

    const timeoutValue = useRef(opts.timeout);

    const [result, setResult] = useState<Result>();
    const [error, setError] = useState<Error>();

    const [triggered, setTriggered] = useState(false);

    const cb = useCallback(() => {
        const timeout = opts.timeout;
        if (typeof timeout === "number") {
            // typescript doesn't know what it is doing here
            return (fn as (
                ...args: [...Arguments, AbortSignal]
            ) => Promise<Result>)(
                ...deps,
                (abortController as AbortController).signal
            );
        } else {
            return (fn as (...args: Arguments) => Promise<Result>)(...deps);
        }
    }, [...deps, opts.timeout, abortController]);

    const runCallback = useCallback(() => {
        cb()
            .then(res => {
                setResult(res);
            })
            .catch(err => {
                setError(err);
            });
    }, [cb]);

    useEffect(() => {
        timeoutValue.current = opts.timeout;
    }, [opts.timeout]);

    useEffect(() => {
        if (!abortController) return;

        const {current: currentTimeoutValue} = timeoutValue;

        // should always be a number by this point
        assert(typeof currentTimeoutValue === "number");

        const timeout = setTimeout(() => {
            abortController.abort();
        }, currentTimeoutValue);

        return () => clearTimeout(timeout);
    }, [abortController, timeoutValue]);

    useEffect(() => {
        if (triggered) {
            runCallback();
            setTriggered(false);
        }
    }, [triggered, setTriggered, runCallback]);

    const run = useCallback(() => {
        setResult(undefined);
        setError(undefined);

        if (typeof opts.timeout === "number")
            setAbortController(new AbortController());

        setTriggered(true);
    }, [cb]);

    const reset = useCallback(() => {
        setResult(undefined);
        setError(undefined);
    }, []);

    return useMemo(
        () =>
            [
                run,
                result,
                error,
                {triggered, reset}
            ] as UseDeferredAsyncResult<Result>,
        [result, run, error, {triggered, reset}]
    );
}

export interface UseAsyncOptions<
    Arguments extends unknown[],
    IsAborting extends Timeout = false
> extends UseDeferredAsyncOptions<IsAborting> {
    filterRequest?(...deps: Arguments): boolean;
}

/***
 * useDeferredAsync, but called immediately
 * @param fn - Async function
 * @param deps - Arguments to `fn`. Will trigger request when these change
 * @param opts - Optional settings for the hook
 * @param opts.filterRequest - Only run a request when this returns true
 */
export function useAsync<
    Result,
    Arguments extends unknown[],
    TimeoutTime extends Timeout
>(
    fn: (
        ...args: TimeoutTime extends number
            ? [...Arguments, AbortSignal]
            : Arguments
    ) => Promise<Result>,
    deps: Arguments,
    opts: UseAsyncOptions<Arguments, TimeoutTime> = {}
): UseAsyncResult<Result> {
    const [run, result, error] = useDeferredAsync(fn, deps, opts);

    const optsRef = useRef(opts);

    useEffect(() => {
        optsRef.current = opts;
    }, [opts]);

    useEffect(() => {
        const {current: options} = optsRef;

        if (options.filterRequest?.(...deps) ?? true) {
            run();
        }
    }, [...deps, optsRef]);

    return useMemo(() => [result, error] as UseAsyncResult<Result>, [
        result,
        error
    ]);
}

export function useMappedAsync<Result, Mapped>(
    useAsyncResult: UseAsyncResult<Result>,
    map: (v: Result) => Mapped
): UseAsyncResult<Mapped> {
    const [result, state] = useAsyncResult;

    const mappedResult = useMemo(
        () =>
            typeof result === "undefined" ? undefined : map(result as Result),
        [result]
    );

    return useMemo(() => [mappedResult, state] as UseAsyncResult<Mapped>, [
        mappedResult,
        state
    ]);
}
