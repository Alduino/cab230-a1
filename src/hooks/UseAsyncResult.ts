type LoadingResult = [undefined, undefined];
type ErrorResult = [undefined, Error];
type CompletedResult<T> = [T, undefined];

type UseAsyncResult<T> = LoadingResult | ErrorResult | CompletedResult<T>;

export default UseAsyncResult;
