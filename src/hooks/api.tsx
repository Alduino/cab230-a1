import React, {
    createContext,
    ReactElement,
    useCallback,
    useContext,
    useMemo,
    useState
} from "react";
import ChildrenProps from "../util/ChildrenProps";
import superSimpleJwtParse from "../util/superSimpleJwtParse";
import {
    useAsync,
    UseAsyncOptions,
    useDeferredAsync,
    UseDeferredAsyncOptions,
    useMappedAsync
} from "./async";
import UseAsyncResult from "./UseAsyncResult";
import UseDeferredAsyncResult from "./UseDeferredAsyncResult";

interface BasicFetchExtensions {
    dontThrowFor?: number[];
    query?: Record<string, string | undefined>;
}

function basicFetch(
    url: string,
    {
        query: queryObject,
        dontThrowFor,
        ...opts
    }: RequestInit & BasicFetchExtensions
): Promise<Response> {
    const query = Object.fromEntries(
        Object.entries(queryObject || {}).filter(([, val]) => !!val)
    ) as Record<string, string>;
    const queryString = new URLSearchParams(query);
    const urlWithQuery = queryObject ? url + "?" + queryString.toString() : url;
    return fetch(urlWithQuery, opts).then(res => {
        if (res.ok || dontThrowFor?.includes(res.status)) return res;
        throw new Error(
            `Response was not successful: ${res.status} ${res.statusText}`
        );
    });
}

function arrayBuff2Hex(src: ArrayBuffer) {
    return Array.from(new Uint8Array(src))
        .map(v => v.toString(16).padStart(2, "0"))
        .join("");
}

/**
 * Hashes the src string with SHA256, returns hex string of hash
 */
async function sha256(src: string) {
    const bytes = new TextEncoder().encode(src);
    const hash = await crypto.subtle.digest("SHA-256", bytes);
    return arrayBuff2Hex(hash);
}

export interface RequestOptions {
    method: string;
    query?: Record<string, string | undefined>;
    body?: RequestInit["body"];
    headers?: RequestInit["headers"];
    cache?: boolean;
    auth?: boolean;
    // list of non-ok response codes to not throw an error for
    dontThrowFor?: number[];
}

interface ApiContext {
    request<T>(
        path: string,
        opts: RequestOptions,
        signal: AbortSignal
    ): Promise<T>;

    getAuthToken(): string | null;
    setAuthToken(token: string | null): void;
}

function throwNotWrapped(): never {
    throw new Error("You forgot to wrap the <ApiProvider />");
}

const ApiContext = createContext<ApiContext>({
    request() {
        throwNotWrapped();
    },
    getAuthToken() {
        throwNotWrapped();
    },
    setAuthToken() {
        throwNotWrapped();
    }
});
ApiContext.displayName = "ApiContext";

export interface ApiProviderProps extends ChildrenProps {
    baseUrl: string;
}

export const ApiProvider = (props: ApiProviderProps): ReactElement => {
    const lsKey = "auth.token";

    const [token, setToken] = useState<string | null>(
        localStorage.getItem(lsKey)
    );

    const getAuthToken = useCallback(() => {
        if (!token) {
            return null;
        }

        const {exp} = superSimpleJwtParse(token);

        if (Date.now() / 1000 > exp) {
            // token has expired
            localStorage.removeItem("auth.token");
            setToken(null);
            return null;
        }

        return token;
    }, [token]);

    const setAuthToken = useCallback((token: string | null) => {
        setToken(token);
        if (token) localStorage.setItem("auth.token", token);
        else localStorage.removeItem("auth.token");
    }, []);

    const sendRequest = useCallback(
        async (
            path: string,
            {
                cache: useCache = true,
                auth: useAuth = false,
                ...opts
            }: RequestOptions,
            signal: AbortSignal
        ) => {
            const url = new URL(path, props.baseUrl).toString();

            const overrideHeaders = new Map<string, string>();

            const authToken = getAuthToken();
            if (useAuth && authToken) {
                overrideHeaders.set("Authorization", `Bearer ${authToken}`);
            }

            const reqOpts: RequestInit & BasicFetchExtensions = {
                ...opts,
                headers: {
                    ...opts.headers,
                    ...Object.fromEntries(overrideHeaders.entries())
                }
            };

            // key must be hashed, as it can contain sensitive data
            const cacheKey = JSON.stringify(reqOpts);
            const hashedCacheKey = `${url}?opts=${await sha256(cacheKey)}`;

            if (useCache) {
                const cache = await caches.open("api-cache");

                let response = await cache.match(hashedCacheKey);
                if (!response) {
                    const res = await basicFetch(url, {...reqOpts, signal});
                    response = res.clone();
                    await cache.put(hashedCacheKey, res);
                }

                return response;
            } else {
                return await basicFetch(url, {...opts, signal});
            }
        },
        [props.baseUrl, getAuthToken]
    );

    const sendAndReadRequest = useCallback(
        async (path: string, opts: RequestOptions, signal: AbortSignal) => {
            try {
                const req = await sendRequest(path, opts, signal);
                return await req.json();
            } catch (err) {
                throw new Error(
                    `Could not send or parse API request: ${err.message}`
                );
            }
        },
        [sendRequest]
    );

    const valueMemo = useMemo<ApiContext>(
        () => ({
            request: sendAndReadRequest,
            getAuthToken,
            setAuthToken
        }),
        [sendRequest, getAuthToken, setAuthToken]
    );

    return (
        <ApiContext.Provider value={valueMemo}>
            {props.children}
        </ApiContext.Provider>
    );
};

/**
 * Sends an API request, relative to the baseUrl set on the ApiProvider
 * @param path - Relative path to request
 * @param opts - RequestInit + query object - should be wrapped in useMemo
 * @param asyncOpts - Options for useAsync hook
 */
export function useApi<T>(
    path: string,
    opts: RequestOptions,
    asyncOpts?: UseAsyncOptions<[string, RequestOptions]>
): UseAsyncResult<T> {
    const ctx = useContext(ApiContext);
    return useAsync<T, [string, RequestOptions], number>(
        ctx.request,
        [path, opts],
        {
            ...asyncOpts,
            timeout: 1000
        }
    ) as UseAsyncResult<T>;
}

export function useDeferredApi<T>(
    path: string,
    opts: RequestOptions,
    asyncOpts?: UseDeferredAsyncOptions
): UseDeferredAsyncResult<T> {
    const ctx = useContext(ApiContext);
    return useDeferredAsync<T, [string, RequestOptions], number>(
        ctx.request,
        [path, opts],
        {...asyncOpts, timeout: 1000}
    );
}

export function useAuthenticationStatus(): boolean {
    const ctx = useContext(ApiContext);
    return !!ctx.getAuthToken();
}

export function useAuthenticatedEmail(): string | null {
    const ctx = useContext(ApiContext);
    const jwt = ctx.getAuthToken();
    if (!jwt) return null;
    const {email} = superSimpleJwtParse(jwt);
    return email;
}

interface LogInRequest {
    email: string;
    password: string;
}

interface LoginResult {
    token: string;
    token_type: "Bearer";
    expires_in: number;
}

interface ErrorResult {
    error: true;
}

function isErrorResult(res: unknown): res is ErrorResult {
    return (
        typeof res === "object" &&
        res !== null &&
        "error" in res &&
        (res as ErrorResult).error
    );
}

async function handleLogin(
    ctx: ApiContext,
    data: LogInRequest,
    signal: AbortSignal
): Promise<boolean> {
    const result = await ctx.request<LoginResult | ErrorResult>(
        "user/login",
        {
            method: "POST",
            body: JSON.stringify(data),
            headers: {"Content-Type": "application/json"},
            cache: false,
            dontThrowFor: [401]
        },
        signal
    );

    if (isErrorResult(result)) {
        return false;
    }

    ctx.setAuthToken(result.token);
    return true;
}

export function useDeferredLogIn(
    data: LogInRequest,
    asyncOpts?: UseDeferredAsyncOptions
): UseDeferredAsyncResult<boolean> {
    const ctx = useContext(ApiContext);
    return useDeferredAsync<boolean, [ApiContext, LogInRequest], number>(
        handleLogin,
        [ctx, data],
        {
            ...asyncOpts,
            timeout: 1000
        }
    );
}

export function useDeferredLogOut(): () => void {
    const ctx = useContext(ApiContext);
    return useCallback(() => ctx.setAuthToken(null), [ctx.setAuthToken]);
}

interface RegisterRequest {
    email: string;
    password: string;
}

interface RegisterResult {
    error?: true;
    message: string;
}

async function handleRegister(
    ctx: ApiContext,
    data: RegisterRequest,
    signal: AbortSignal
): Promise<boolean> {
    const result = await ctx.request<RegisterResult>(
        "user/register",
        {
            method: "POST",
            body: JSON.stringify(data),
            headers: {"Content-Type": "application/json"},
            cache: false,
            dontThrowFor: [409]
        },
        signal
    );

    return !isErrorResult(result);
}

export function useDeferredRegister(
    data: RegisterRequest,
    asyncOpts?: UseDeferredAsyncOptions
): UseDeferredAsyncResult<boolean> {
    const ctx = useContext(ApiContext);
    return useDeferredAsync<boolean, [ApiContext, RegisterRequest], number>(
        handleRegister,
        [ctx, data],
        {
            ...asyncOpts,
            timeout: 1000
        }
    );
}

export function useCountries(
    asyncOpts?: UseAsyncOptions<[string, RequestOptions]>
): UseAsyncResult<string[]> {
    const opts = useMemo(() => ({method: "GET"}), []);
    return useApi("countries", opts, asyncOpts);
}

interface UseRankingsFilter {
    year?: number;
    country?: string;
}

interface ApiRanking {
    rank: number;
    country: string;
    score: string;
    year: number;
}

export interface Ranking {
    rank: number;
    country: string;
    score: number;
    year: number;
}

export function useRankings(
    {year, country}: UseRankingsFilter,
    asyncOpts?: UseAsyncOptions<[string, RequestOptions]>
): UseAsyncResult<Ranking[]> {
    const opts = useMemo(
        () => ({method: "GET", query: {year: year?.toString(), country}}),
        [year, country]
    );

    const response = useApi<ApiRanking[]>("rankings", opts, asyncOpts);
    return useMappedAsync(response, v =>
        v.map(item => ({...item, score: parseFloat(item.score)}))
    );
}

export interface UseFactorsFilter {
    year: number;
    limit?: number;
    country?: string;
}

interface ApiRankingWithFactors {
    rank: number;
    country: string;
    score: string;
    economy: string;
    family: string;
    health: string;
    freedom: string;
    generosity: string;
    trust: string;
}

export interface RankingWithFactors {
    rank: number;
    country: string;
    score: number;
    economy: number;
    family: number;
    health: number;
    freedom: number;
    generosity: number;
    trust: number;
}

export function useFactors(
    {year, limit, country}: UseFactorsFilter,
    asyncOpts?: UseAsyncOptions<[string, RequestOptions]>
): UseAsyncResult<RankingWithFactors[]> {
    const opts = useMemo<RequestOptions>(
        () => ({
            method: "GET",
            query: {limit: limit?.toString(), country},
            auth: true
        }),
        [limit, country]
    );

    const result = useApi<ApiRankingWithFactors[]>(
        `factors/${year}`,
        opts,
        asyncOpts
    );
    return useMappedAsync(result, arr =>
        arr.map(item => ({
            ...item,
            score: parseFloat(item.score),
            economy: parseFloat(item.economy),
            family: parseFloat(item.family),
            health: parseFloat(item.health),
            freedom: parseFloat(item.freedom),
            generosity: parseFloat(item.generosity),
            trust: parseFloat(item.trust)
        }))
    );
}
