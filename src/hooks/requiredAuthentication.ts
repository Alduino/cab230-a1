import {useEffect} from "react";
import {useHistory} from "react-router";
import {useAuthenticationStatus} from "./api";

export default function useRequiredAuthentication() {
    const isLoggedIn = useAuthenticationStatus();
    const {push, location} = useHistory();

    useEffect(() => {
        if (isLoggedIn) return;
        push(`/account/login?return=${location.pathname}`);
    }, [isLoggedIn, push]);
}
