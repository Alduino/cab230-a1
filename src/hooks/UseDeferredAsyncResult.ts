type WaitingResult = [
    () => void,
    undefined,
    undefined,
    {triggered: false; reset(): void}
];
type LoadingResult = [
    () => void,
    undefined,
    undefined,
    {triggered: true; reset(): void}
];
type ErrorResult = [
    () => void,
    undefined,
    Error,
    {triggered: false; reset(): void}
];
type CompletedResult<T> = [
    () => void,
    T,
    undefined,
    {triggered: false; reset(): void}
];

type UseDeferredAsyncResult<T> =
    | WaitingResult
    | LoadingResult
    | ErrorResult
    | CompletedResult<T>;

export default UseDeferredAsyncResult;
