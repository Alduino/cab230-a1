import {RefObject, useEffect, useState} from "react";

export default function useTextWidth(
    text: string,
    sourceRef: RefObject<HTMLElement>
): number {
    const [measureElem, setMeasureElem] = useState<HTMLSpanElement>();
    const [width, setWidth] = useState<number>(0);

    useEffect(() => {
        const {current: source} = sourceRef;
        if (!source) return;
        const targetElem = document.createElement("span");
        const computedStyles = getComputedStyle(source);

        const fontStyleKeys = Object.values(computedStyles).filter(v =>
            v.startsWith("font")
        );

        for (const key of fontStyleKeys) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            targetElem.style[key] = computedStyles[key];
        }

        targetElem.style.position = "fixed";
        targetElem.style.top = "10px";
        targetElem.style.left = "10px";
        targetElem.style.opacity = "0";
        targetElem.style.pointerEvents = "none";

        document.body.appendChild(targetElem);
        setMeasureElem(targetElem);

        return () => targetElem.remove();
    }, [sourceRef]);

    useEffect(() => {
        if (!measureElem) return;

        measureElem.textContent = text;
        setWidth(measureElem.scrollWidth);
    }, [text, measureElem]);

    return width;
}
