import {Center, Spinner} from "@chakra-ui/react";
import React, {lazy, ReactElement, Suspense} from "react";
import {Route, Switch} from "react-router";
import {BrowserRouter} from "react-router-dom";
import {Layout} from "./components/Layout";

export const App = (): ReactElement => (
    <BrowserRouter>
        <Layout>
            <Suspense
                fallback={
                    <Center>
                        <Spinner />
                    </Center>
                }
            >
                <Switch>
                    <Route
                        exact
                        path="/"
                        component={lazy(() => import("./routes/LandingPage"))}
                    />
                    <Route
                        exact
                        path="/rankings"
                        component={lazy(() => import("./routes/RankingsPage"))}
                    />
                    <Route
                        exact
                        path="/rankings/:country"
                        component={lazy(
                            () => import("./routes/CountryRankingPage")
                        )}
                    />
                    <Route
                        exact
                        path="/account/login"
                        component={lazy(() => import("./routes/account/LogIn"))}
                    />
                    <Route
                        exact
                        path="/account/logout"
                        component={lazy(
                            () => import("./routes/account/Logout")
                        )}
                    />
                    <Route
                        exact
                        path="/account/register"
                        component={lazy(
                            () => import("./routes/account/Register")
                        )}
                    />
                    <Route
                        exact
                        path="/factors"
                        component={lazy(() => import("./routes/FactorsPage"))}
                    />
                </Switch>
            </Suspense>
        </Layout>
    </BrowserRouter>
);
