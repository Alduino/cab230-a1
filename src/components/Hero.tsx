import {Box, chakra, useColorModeValue} from "@chakra-ui/react";
import React from "react";
import ChildrenProps from "../util/ChildrenProps";
import ClassNameProps from "../util/ClassNameProps";
import {GradientBorderBox} from "./GradientBorderBox";

export const Hero = chakra((props: ChildrenProps & ClassNameProps) => {
    const bg = useColorModeValue("white", "gray.800");

    return (
        <GradientBorderBox
            gradient="linear(to-br, green.500, blue.500)"
            background={bg}
            thickness={1}
            borderRadius="lg"
        >
            <Box p={4} className={props.className}>
                {props.children}
            </Box>
        </GradientBorderBox>
    );
});
