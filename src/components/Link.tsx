import {chakra, Link as ChakraLink, useColorModeValue} from "@chakra-ui/react";
import React, {ReactElement, useCallback} from "react";
import {useHistory} from "react-router-dom";
import ChildrenProps from "../util/ChildrenProps";
import ClassNameProps from "../util/ClassNameProps";

export interface LinkProps extends ChildrenProps, ClassNameProps {
    href: string;
    useDefaultStyle?: boolean;

    // sets the data-active prop
    isActive?: boolean;

    // `true` return value disables default action
    onClick?(): void | boolean;
}

export const Link = chakra(function Link(props: LinkProps): ReactElement {
    const history = useHistory();

    const colour = useColorModeValue("blue.500", "blue.200");
    const hoverColour = useColorModeValue("blue.600", "blue.100");

    const extraProps =
        props.useDefaultStyle === false
            ? {}
            : {
                  color: colour,
                  _hover: {
                      textDecoration: "underline",
                      color: hoverColour
                  }
              };

    const handleClick = useCallback(
        (e: {preventDefault(): void}) => {
            e.preventDefault();
            if (!props.onClick?.()) history.push(props.href);
        },
        [history.push]
    );

    return (
        <ChakraLink
            href={props.href}
            className={props.className}
            onClick={handleClick}
            data-active={props.isActive ? true : undefined}
            {...extraProps}
        >
            {props.children}
        </ChakraLink>
    );
});
