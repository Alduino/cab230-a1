import {Box, useToken} from "@chakra-ui/react";
import React, {ReactElement} from "react";
import ChildrenProps from "../util/ChildrenProps";

export interface GradientBorderBoxProps extends ChildrenProps {
    gradient: string;
    background: string;
    thickness: number | string;
    borderRadius?: string | number;
    className?: string;
}

export const GradientBorderBox = (
    props: GradientBorderBoxProps
): ReactElement => {
    const size = useToken("sizes", props.thickness);

    return (
        <Box
            borderRadius={props.borderRadius}
            position="relative"
            border={`solid ${size} transparent`}
            bg={props.background}
            backgroundClip="padding-box"
            boxSizing="border-box"
            className={props.className}
            _before={{
                content: "''",
                position: "absolute",
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                zIndex: -1,
                margin: `-${size}`,
                borderRadius: "inherit",
                bgGradient: props.gradient
            }}
        >
            {props.children}
        </Box>
    );
};
