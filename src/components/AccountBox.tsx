import {
    Box,
    Button,
    Center,
    Divider,
    Heading,
    Input,
    Text,
    useColorModeValue,
    VStack
} from "@chakra-ui/react";
import React, {ReactElement, useCallback, useEffect} from "react";
import {BiError} from "react-icons/bi";
import {IoMdCheckmark} from "react-icons/io";
import {useHistory} from "react-router";
import UseDeferredAsyncResult from "../hooks/UseDeferredAsyncResult";
import {ErrorText} from "./ErrorText";
import {Form, FormAction, FormInput, FormItem, FormLabel} from "./Form";

export interface AccountBoxProps {
    title: string;
    description: string;
    completeLabel: string;
    errorLabel: string;
    successRedirect: string;
    deferredHandler: UseDeferredAsyncResult<boolean>;
    onFieldsChanged(fields: Record<string, string>): void;
}

export const AccountBox = ({
    title,
    description,
    completeLabel,
    errorLabel,
    successRedirect,
    deferredHandler,
    onFieldsChanged
}: AccountBoxProps): ReactElement => {
    const backgroundColour = useColorModeValue("gray.50", "gray.700");
    const {push: historyPush} = useHistory();

    const [trigger, result, error, {triggered, reset}] = deferredHandler;

    const handleRedirect = useCallback(() => historyPush(successRedirect), [
        historyPush,
        successRedirect
    ]);

    const handleFormSubmit = useCallback(() => trigger(), [trigger]);
    const handleFormChanged = useCallback(onFieldsChanged, [onFieldsChanged]);

    // Hide error message after 2sec
    useEffect(() => {
        if (result !== false) return;
        const timeout = setTimeout(reset, 2000);
        return () => clearTimeout(timeout);
    }, [result, reset]);

    // Redirect to completion page when successful after 2sec
    useEffect(() => {
        if (result !== true) return;
        const timeout = setTimeout(handleRedirect, 2000);
        return () => clearTimeout(timeout);
    });

    return (
        <Center>
            <Box
                bg={backgroundColour}
                w="26rem"
                borderRadius="md"
                overflow="hidden"
            >
                <VStack px={4} py={8} spacing={4}>
                    <VStack>
                        <Heading size="md">{title}</Heading>
                        <Text textAlign="center" fontSize="md" opacity={0.7}>
                            {description}
                        </Text>
                    </VStack>
                    <Divider />
                    <Form
                        gridGap={4}
                        onSubmit={handleFormSubmit}
                        onChange={handleFormChanged}
                    >
                        <FormItem id="email">
                            <FormLabel>
                                <Text>Email:</Text>
                            </FormLabel>
                            <FormInput>
                                {props => <Input {...props} type="email" />}
                            </FormInput>
                        </FormItem>
                        <FormItem id="password">
                            <FormLabel>
                                <Text>Password:</Text>
                            </FormLabel>
                            <FormInput>
                                {props => <Input {...props} type="password" />}
                            </FormInput>
                        </FormItem>
                        <FormAction>
                            {error ? (
                                <Button
                                    colorScheme="red"
                                    isFullWidth={true}
                                    leftIcon={<BiError />}
                                    disabled
                                >
                                    Something went wrong
                                </Button>
                            ) : result === true ? (
                                <Button
                                    colorScheme="green"
                                    isFullWidth={true}
                                    leftIcon={<IoMdCheckmark />}
                                    disabled
                                >
                                    {completeLabel}
                                </Button>
                            ) : result === false ? (
                                <Button
                                    colorScheme="red"
                                    isFullWidth={true}
                                    leftIcon={<BiError />}
                                    disabled
                                >
                                    {errorLabel}
                                </Button>
                            ) : triggered ? (
                                <Button
                                    colorScheme="green"
                                    isFullWidth={true}
                                    type="submit"
                                    isLoading
                                />
                            ) : (
                                <Button
                                    colorScheme="green"
                                    isFullWidth={true}
                                    type="submit"
                                >
                                    Submit
                                </Button>
                            )}
                        </FormAction>
                    </Form>
                </VStack>

                {process.env.NODE_ENV !== "production" && error && (
                    <Box width="full">
                        <ErrorText error={error} />
                    </Box>
                )}
            </Box>
        </Center>
    );
};
