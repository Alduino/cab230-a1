import {Box, Divider, Heading, HStack} from "@chakra-ui/react";
import React, {ReactElement} from "react";
import ChildrenProps from "../util/ChildrenProps";

export interface HeroItemProps extends ChildrenProps {
    icon?: ReactElement;
    heading: string;
}

export const HeroItem = (props: HeroItemProps): ReactElement => (
    <Box flex="1 1 0">
        <HStack mb={4}>
            {props.icon}
            <Heading size="sm">{props.heading}</Heading>
        </HStack>
        {props.children}
    </Box>
);

export const VerticalDivider = (): ReactElement => (
    <Box>
        <Divider orientation="vertical" />
    </Box>
);
