import {chakra, Flex, Grid} from "@chakra-ui/react";
import React, {
    createContext,
    ReactElement,
    ReactNode,
    ReactNodeArray,
    SyntheticEvent,
    useCallback,
    useContext,
    useEffect,
    useMemo,
    useState
} from "react";
import ChildrenProps from "../util/ChildrenProps";
import ClassNameProps from "../util/ClassNameProps";

interface ResultContext {
    (id: string, value: string): void;
}

const ResultContext = createContext<ResultContext>(() => {
    throw new Error("FormInput must be wrapped in <Form />");
});
ResultContext.displayName = "Form.ResultContext";

const RowContext = createContext(0);
RowContext.displayName = "Form.RowContext";

const IdContext = createContext("");
IdContext.displayName = "Form.IdContext";

export interface FormProps extends ClassNameProps {
    children: ReactNodeArray | ReactNode;

    onSubmit(values: Record<string, string>): void;
    onChange?(values: Record<string, string>): void;
}

export const Form = chakra(
    ({children, className, onSubmit, onChange}: FormProps): ReactElement => {
        const valueMap = useMemo(() => new Map<string, string>(), []);

        const handleResultSet = useCallback(
            (k: string, v: string) => {
                valueMap.set(k, v);
                onChange?.(Object.fromEntries(valueMap.entries()));
            },
            [valueMap]
        );

        return (
            <Grid
                as="form"
                templateColumns="auto 1fr"
                templateRows="repeat(auto-fill, 1fr)"
                alignItems="center"
                className={className}
                onSubmit={e => {
                    e.preventDefault();
                    onSubmit(Object.fromEntries(valueMap.entries()));
                }}
            >
                <ResultContext.Provider value={handleResultSet}>
                    {(Array.isArray(children) ? children : [children]).map(
                        (child, i) => (
                            <RowContext.Provider key={i} value={i + 1}>
                                {child}
                            </RowContext.Provider>
                        )
                    )}
                </ResultContext.Provider>
            </Grid>
        );
    }
);

interface FormItemProps extends ChildrenProps {
    id: string;
}

export const FormItem = ({children, id}: FormItemProps): ReactElement => (
    <IdContext.Provider value={id}>{children}</IdContext.Provider>
);

export const FormLabel = ({
    children,
    className
}: ChildrenProps & ClassNameProps): ReactElement => {
    const row = useContext(RowContext);
    const id = useContext(IdContext);

    return (
        <chakra.label
            htmlFor={id}
            gridRow={row}
            gridColumn={1}
            className={className}
            justifySelf="end"
        >
            {children}
        </chakra.label>
    );
};

export interface FormInputChildProps {
    id: string;
    className?: string;
}

export const FormInput = ({
    children,
    childrenDeps
}: {
    children(props: FormInputChildProps): ReactElement;
    childrenDeps?: unknown[];
}): ReactElement => {
    const row = useContext(RowContext);
    const id = useContext(IdContext);
    const setResult = useContext(ResultContext);

    const [value, setValue] = useState("");

    const handleChange = useCallback(
        (ev: SyntheticEvent<HTMLInputElement> | string) => {
            if (typeof ev === "string") {
                setValue(ev);
            } else {
                ev.preventDefault();
                setValue(ev.currentTarget.value);
            }
        },
        [id, setValue]
    );

    useEffect(() => {
        setResult(id, value);
    }, [setResult, id, value]);

    const ChakraChildren = useMemo(() => chakra(children), childrenDeps ?? []);

    return (
        <ChakraChildren
            id={id}
            value={value}
            onChange={handleChange}
            gridRow={row}
            justifySelf="start"
        />
    );
};

export const FormAction = ({children}: ChildrenProps): ReactElement => {
    const row = useContext(RowContext);
    return (
        <Flex gridRow={row} gridColumn={2}>
            {children}
        </Flex>
    );
};
