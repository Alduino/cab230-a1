import {
    Box,
    Button,
    HStack,
    Icon,
    Input,
    Modal,
    ModalContent,
    ModalOverlay,
    Stack,
    Text,
    useColorModeValue,
    chakra
} from "@chakra-ui/react";
import React, {
    ChangeEvent,
    KeyboardEvent,
    ReactElement,
    useCallback,
    useEffect,
    useMemo,
    useRef,
    useState
} from "react";
import {BiSearch} from "react-icons/bi";
import {useHistory} from "react-router";
import {useCountries} from "../hooks/api";
import useTextWidth from "../hooks/textWidth";
import modulo from "../util/modulo";
import {ErrorText} from "./ErrorText";
import {Link} from "./Link";

interface SearchPopupProps {
    isOpen: boolean;
    onClose(): void;
}

const SearchPopupInner = ({onClose}: {onClose(): void}) => {
    const inputRef = useRef<HTMLInputElement>(null);
    const autocompleteRef = useRef<HTMLParagraphElement>(null);

    const [searchText, setSearchText] = useState("");
    const [selectedIndex, setSelectedIndex] = useState(0);

    const activeColour = useColorModeValue("blackAlpha.200", "whiteAlpha.200");
    const hoverColour = useColorModeValue("blackAlpha.100", "whiteAlpha.100");

    const {push: historyPush} = useHistory();

    const [countries, countriesError] = useCountries();

    const handleSearchChanged = useCallback(
        (ev: ChangeEvent<HTMLInputElement>) =>
            setSearchText(ev.target.value.trimStart()),
        [setSearchText]
    );

    const trimmedSearch = searchText.trimEnd();

    // sort by length, smallest to largest
    const countriesSorted = useMemo(
        () => countries?.sort((a, b) => a.length - b.length),
        [countries]
    );

    const matchingNames = useMemo(
        () =>
            countriesSorted?.filter(
                el =>
                    searchText &&
                    el.toLowerCase().startsWith(searchText.toLowerCase())
            ) ?? [],
        [countriesSorted, searchText]
    );

    useEffect(() => {
        setSelectedIndex(0);
    }, [setSelectedIndex, matchingNames.length]);

    const firstMatchingName = matchingNames[0];

    const handleInputFocus = useCallback(() => inputRef.current?.focus(), [
        inputRef.current
    ]);

    const handleSearchKeyDown = useCallback(
        (ev: KeyboardEvent<HTMLInputElement>) => {
            if (ev.key === "Tab" && firstMatchingName) {
                ev.preventDefault();
                setSearchText(firstMatchingName);
            } else if (ev.key === "ArrowDown") {
                ev.preventDefault();
                setSelectedIndex(prev => (prev + 1) % matchingNames.length);
            } else if (ev.key === "ArrowUp") {
                ev.preventDefault();
                setSelectedIndex(prev =>
                    modulo(prev - 1, matchingNames.length)
                );
            } else if (ev.key === "Enter") {
                ev.preventDefault();
                const selected = matchingNames[selectedIndex];
                if (!selected) return;
                onClose();
                historyPush(`/rankings/${selected}`);
            }
        },
        [
            setSearchText,
            firstMatchingName,
            matchingNames,
            selectedIndex,
            onClose,
            historyPush
        ]
    );

    const searchTextWidth = useTextWidth(trimmedSearch, inputRef);

    if (countriesError) {
        return <ErrorText error={countriesError} />;
    }

    return (
        <Box>
            <HStack
                cursor="text"
                px={8}
                spacing={4}
                fontSize="lg"
                onClick={handleInputFocus}
            >
                <Icon as={BiSearch} />
                <Box position="relative" flexGrow={1}>
                    <Input
                        variant="unstyled"
                        size="lg"
                        py={searchText.length === 0 ? 6 : 4}
                        placeholder="Find a country"
                        autoComplete="off"
                        value={searchText}
                        onChange={handleSearchChanged}
                        onKeyDown={handleSearchKeyDown}
                        ref={inputRef}
                    />
                    <Text
                        position="absolute"
                        top={searchText.length === 0 ? 6 : 4}
                        style={{left: searchTextWidth}}
                        whiteSpace="pre"
                        opacity={0.5}
                        pointerEvents="none"
                        userSelect="none"
                        transition="top 0.2s"
                        ref={autocompleteRef}
                    >
                        {firstMatchingName?.substring(trimmedSearch.length)}
                    </Text>
                </Box>
            </HStack>

            {searchText && (
                <Stack spacing={0} pb={2}>
                    {matchingNames.map((name, i) => (
                        <Link
                            px={8}
                            py={2}
                            isActive={i === selectedIndex}
                            _hover={{
                                background: hoverColour,
                                textDecoration: "none"
                            }}
                            _active={{background: activeColour}}
                            transition="none"
                            onClick={onClose}
                            href={`/rankings/${name}`}
                            useDefaultStyle={false}
                            key={i}
                        >
                            <chakra.span fontWeight="bold">
                                {name.substring(0, searchText.length)}
                            </chakra.span>
                            {name.substring(searchText.length)}
                        </Link>
                    ))}

                    {matchingNames.length === 0 && (
                        <Text px={8} py={2}>
                            No results for that search
                        </Text>
                    )}
                </Stack>
            )}
        </Box>
    );
};

const SearchPopup = ({isOpen, onClose}: SearchPopupProps) => (
    <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
            <SearchPopupInner onClose={onClose} />
        </ModalContent>
    </Modal>
);

const SearchBox = ({onClick}: {onClick(): void}) => {
    const textColour = useColorModeValue("blackAlpha.600", "white");

    return (
        <Box py={3}>
            <HStack
                as={Button}
                role="search"
                borderRadius="md"
                border="1px"
                borderColor="white"
                opacity={0.5}
                w={64}
                h="full"
                px={2}
                cursor="pointer"
                tabIndex={0}
                userSelect="none"
                justifyContent="start"
                color={textColour}
                onClick={onClick}
            >
                <BiSearch />
                <Text>Find a country</Text>
            </HStack>
        </Box>
    );
};

export const Search = (): ReactElement => {
    const [isPopupOpen, setIsPopupOpen] = useState(false);

    return (
        <>
            <SearchBox onClick={() => setIsPopupOpen(true)} />
            <SearchPopup
                isOpen={isPopupOpen}
                onClose={() => setIsPopupOpen(false)}
            />
        </>
    );
};
