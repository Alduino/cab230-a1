import {
    Box,
    Center,
    Divider,
    Grid,
    HStack,
    LinkOverlay,
    Text
} from "@chakra-ui/react";
import React, {ReactElement} from "react";
import {useAuthenticatedEmail, useAuthenticationStatus} from "../hooks/api";
import ChildrenProps from "../util/ChildrenProps";
import {Link} from "./Link";
import {
    NavigationBar,
    NavigationBarContainer,
    NavigationItem
} from "./NavigationBar";
import {Search} from "./Search";

const Dot = (): ReactElement => <Text>·</Text>;

const LoggedInDisplay = (): ReactElement => {
    const email = useAuthenticatedEmail();

    return (
        <>
            <Text alignSelf="center">{email}:</Text>
            <NavigationItem label="Log out" link="/account/logout" />
        </>
    );
};

const LoggedOutDisplay = (): ReactElement => (
    <>
        <NavigationItem useHoverBg={false} useLinkOverlay={true}>
            <LinkOverlay
                as={Link}
                href="/account/register"
                color="white"
                bg="green.500"
                px={4}
                py={2}
                borderRadius="md"
                _hover={{
                    bg: "green.600",
                    color: "white",
                    textDecoration: "none"
                }}
                _active={{
                    bg: "green.700"
                }}
            >
                Register
            </LinkOverlay>
        </NavigationItem>

        <NavigationItem label="Log in" link="/account/login" />
    </>
);
export const Layout = ({children}: ChildrenProps): ReactElement => {
    const isLoggedIn = useAuthenticationStatus();

    return (
        <Grid
            templateColumns={[
                "0 100% 0",
                null,
                null,
                "1fr 62em 1fr",
                "1fr 75em 1fr"
            ]}
            templateRows="4rem auto auto 4rem"
        >
            <NavigationBarContainer gridColumn="1 / 4" gridRow="1" />

            <NavigationBar title="Hapico Foundation" gridColumn="2" gridRow={1}>
                <Search />

                <Box px={4}>
                    <Divider orientation="vertical" />
                </Box>

                <NavigationItem label="Rankings" link="/rankings" />
                <NavigationItem label="Factors" link="/factors" />

                <Box px={4}>
                    <Divider orientation="vertical" />
                </Box>

                {isLoggedIn ? <LoggedInDisplay /> : <LoggedOutDisplay />}
            </NavigationBar>

            <Box gridRow={2} gridColumn={2} my={16}>
                {children}
            </Box>

            <Center opacity={0.6} gridRow={3} gridColumn="1 / 4">
                <HStack as="footer">
                    <Text>© Hapico Foundation 2021</Text>
                    <Dot />
                    <Link href="/">Privacy</Link>
                    <Dot />
                    <Link href="/">Terms of use</Link>
                </HStack>
            </Center>
        </Grid>
    );
};
