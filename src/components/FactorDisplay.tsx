import {Box, chakra, HStack, Text} from "@chakra-ui/react";
import React, {ReactElement, useMemo} from "react";
import ClassNameProps from "../util/ClassNameProps";

interface TopFactors {
    economy: number;
    family: number;
    health: number;
    freedom: number;
    generosity: number;
    trust: number;
}

function useTopFactors(factors: TopFactors) {
    return useMemo(() => {
        const items = [
            {
                name: "Economy",
                color: "yellow.400",
                value: factors.economy,
                accum: 0
            },
            {
                name: "Family",
                color: "blue.600",
                value: factors.family,
                accum: 0
            },
            {name: "Health", color: "red.300", value: factors.health, accum: 0},
            {
                name: "Freedom",
                color: "blue.300",
                value: factors.freedom,
                accum: 0
            },
            {
                name: "Generosity",
                color: "green.300",
                value: factors.generosity,
                accum: 0
            },
            {name: "Trust", color: "green.600", value: factors.trust, accum: 0}
        ].sort((a, b) => b.value - a.value);

        let previousAccum = 0;
        for (const item of items) {
            item.accum = previousAccum;
            previousAccum += item.value;
        }

        return items;
    }, [
        factors.economy,
        factors.family,
        factors.health,
        factors.freedom,
        factors.generosity,
        factors.trust
    ]);
}

export interface FactorDisplayProps extends TopFactors, ClassNameProps {}

export const FactorDisplay = chakra(
    (props: FactorDisplayProps): ReactElement => {
        const items = useTopFactors(props);
        const total = items.reduce((prev, curr) => prev + curr.value, 0);

        return (
            <HStack
                spacing={0}
                borderRadius="md"
                overflow="hidden"
                className={props.className}
            >
                <Box>
                    {items.map((item, i) => (
                        <Text
                            height={8}
                            bg={i % 2 ? "gray.500" : "gray.600"}
                            py={1}
                            pl={2}
                            pr={4}
                            color="white"
                            key={item.name}
                        >
                            {item.name}
                        </Text>
                    ))}
                </Box>
                <Box spacing={0} flexGrow={1}>
                    {items.map((item, i) => (
                        <Box key={i} height={8}>
                            <Box
                                style={{
                                    width: `${(item.accum / total) * 100}%`
                                }}
                                height="full"
                                bg={i % 2 ? "gray.200" : "gray.300"}
                                display="inline-block"
                                transition="width .2s"
                            />
                            <Box
                                style={{
                                    width: `${(item.value / total) * 100}%`
                                }}
                                height="full"
                                bg={item.color}
                                display="inline-block"
                                position="relative"
                                transition="width .2s"
                            >
                                <Text
                                    position="absolute"
                                    right={2}
                                    top={1}
                                    color="white"
                                >
                                    ~{Math.floor((item.value / total) * 100)}%
                                </Text>
                            </Box>
                            <Box
                                style={{
                                    width: `${
                                        (1 -
                                            (item.accum + item.value) / total) *
                                        100
                                    }%`
                                }}
                                height="full"
                                bg={i % 2 ? "gray.200" : "gray.300"}
                                display="inline-block"
                                transition="width .2s"
                            />
                        </Box>
                    ))}
                </Box>
            </HStack>
        );
    }
);
