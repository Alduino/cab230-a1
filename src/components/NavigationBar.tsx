import {
    Box,
    Center,
    chakra,
    Heading,
    HStack,
    LinkBox,
    Text
} from "@chakra-ui/react";
import React, {ReactElement} from "react";
import {useLocation} from "react-router-dom";
import {Logo} from "../Logo";
import ChildrenProps from "../util/ChildrenProps";
import ClassNameProps from "../util/ClassNameProps";
import {Link} from "./Link";

export const NavigationBarContainer = chakra(
    ({children, className}: ChildrenProps & ClassNameProps) => (
        <Box className={className} bg="blue.800">
            {children}
        </Box>
    )
);

export interface NavigationBarProps extends ChildrenProps, ClassNameProps {
    title: string;
}

export const NavigationBar = chakra(
    ({children, className, title}: NavigationBarProps) => (
        <HStack className={className} spacing={4} color="white">
            <Logo alignSelf="stretch" />
            <Link useDefaultStyle={false} href="/">
                <Heading size="md">{title}</Heading>
            </Link>
            <Box flexGrow={1} />
            <HStack align="stretch" alignSelf="stretch" spacing={0}>
                {children}
            </HStack>
        </HStack>
    )
);

interface NavigationItemPropsBase {
    /**
     * When set to false, disables the lighter background on hover
     */
    useHoverBg?: boolean;
}

interface NavigationItemPropsWithLinkOverlay {
    /**
     * Replaces normal link with a `<LinkBox>`, allowing you to use a `<LinkOverlay>` inside.
     * Note that this means it is up to you to use a `<Link>` somewhere.
     */
    useLinkOverlay: true;

    link?: never;
}

interface NavigationItemPropsWithoutLinkOverlay {
    /**
     * Replaces normal link with a `<LinkBox>`, allowing you to use a `<LinkOverlay>` inside.
     * Note that this means it is up to you to use a `<Link>` somewhere.
     */
    useLinkOverlay?: false;

    link: string;
}

interface NavigationItemPropsWithChildren extends ChildrenProps {
    label?: never;
}

interface NavigationItemPropsWithLabel {
    children?: never;
    label: string;
}

export type NavigationItemProps = NavigationItemPropsBase &
    (NavigationItemPropsWithChildren | NavigationItemPropsWithLabel) &
    (
        | NavigationItemPropsWithLinkOverlay
        | NavigationItemPropsWithoutLinkOverlay
    );

export const NavigationItem = (props: NavigationItemProps): ReactElement => {
    const {pathname} = useLocation();

    const baseProps = {
        display: "flex",
        alignItems: "center",
        px: 2,
        _hover: {
            background: props.useHoverBg !== false ? "#fff3" : undefined,
            textDecoration: "none"
        }
    };

    const activeProps =
        pathname === props.link
            ? {
                  background: props.useHoverBg !== false ? "#fff2" : undefined
              }
            : {};

    const inner =
        typeof props.children === "undefined" ? (
            <Text>{props.label}</Text>
        ) : (
            props.children
        );

    return props.useLinkOverlay ? (
        <LinkBox {...baseProps}>{inner}</LinkBox>
    ) : (
        <Link
            {...baseProps}
            {...activeProps}
            useDefaultStyle={false}
            href={props.link}
        >
            {inner}
        </Link>
    );
};
