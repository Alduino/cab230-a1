import {Button, chakra, Flex, Portal, VisuallyHidden} from "@chakra-ui/react";
import React, {
    ChangeEvent,
    ReactElement,
    ReactNode,
    useCallback,
    useMemo,
    useRef
} from "react";
import ClassNameProps from "../util/ClassNameProps";

export interface BSButtonProps {
    isActive: boolean;
    onClick(): void;

    children: ReactNode;
    className?: string;
}

const DefaultButton = ({
    isActive,
    onClick,
    className,
    children
}: BSButtonProps) => (
    <Button
        px={8}
        _active={{
            background: "blue.500",
            color: "white"
        }}
        _focus={{
            zIndex: 1,
            boxShadow: "outline"
        }}
        borderRadius={0}
        className={className}
        isActive={isActive}
        onClick={onClick}
    >
        {children}
    </Button>
);

export interface ButtonSliderProps extends ClassNameProps {
    label: string;

    // defaults to zero
    min?: number;
    max: number;

    // defaults to one
    step?: number;

    // defaults to horizontal
    orientation?: "horizontal" | "vertical";

    value: number;
    onChange?(value: number): void;

    /**
     * Class name for container element
     */
    className?: string;

    /**
     * Override the component used for the button, to allow for custom styles
     */
    button?(props: BSButtonProps): ReactElement;

    /**
     * Override the children of each button. Defaults to displaying value
     * @param value - Value of the slider at this button
     */
    children?(value: number): ReactNode;
}

export const ButtonSlider = chakra(
    ({
        min = 0,
        max,
        step = 1,
        label,
        orientation = "horizontal",
        value,
        onChange,
        className,
        button = DefaultButton,
        children = v => v
    }: ButtonSliderProps): ReactElement => {
        const sliderRef = useRef<HTMLInputElement>(null);

        const handleChange = useCallback(
            (value: number) => {
                onChange?.(value);
                sliderRef.current?.focus();
            },
            [sliderRef, onChange]
        );

        const handleInputChange = useCallback(
            (ev: ChangeEvent<HTMLInputElement>) => {
                handleChange(parseFloat(ev.currentTarget.value));
            },
            [handleChange]
        );

        const ChakrafiedButton = useMemo(() => chakra(button), [button]);

        const totalButtons = Math.floor((max - min) / step) + 1;

        return (
            <>
                <Portal>
                    <VisuallyHidden>
                        <input
                            type="range"
                            aria-label={label}
                            min={min}
                            max={max}
                            step={step}
                            value={value}
                            onChange={handleInputChange}
                            ref={sliderRef}
                        />
                    </VisuallyHidden>
                </Portal>

                <Flex
                    direction={orientation === "vertical" ? "column" : "row"}
                    position="relative"
                    borderRadius="md"
                    overflow="hidden"
                    className={className}
                >
                    {Array.from({length: totalButtons}, (_, i) => {
                        const targetValue =
                            orientation === "vertical"
                                ? max - i * step
                                : min + i * step;

                        return (
                            <ChakrafiedButton
                                key={targetValue}
                                flexGrow={1}
                                _focus={{zIndex: 1, boxShadow: "outline"}}
                                isActive={targetValue === value}
                                onClick={() => handleChange(targetValue)}
                            >
                                {children(targetValue)}
                            </ChakrafiedButton>
                        );
                    })}
                </Flex>
            </>
        );
    }
);
